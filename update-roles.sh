#!/bin/bash

wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "We are Online- Updating Roles"
    rm -R /etc/ansible/roles/*
    ansible-galaxy install -r /etc/ansible/requirements.yml -p /etc/ansible/roles/ 
    ansible-galaxy collection install community.mysql
    ansible-galaxy collection install community.general
    git pull
    /etc/ansible/move-repo.sh
else
    echo "Leider kein Internetverbindung"
fi